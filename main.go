package main

import "fmt"

func main() {

	// calculate fibonacci sequence to specified length with standard loop
	fibList := fibSeq(15)

	//print sequence noting which values are even/odd
	fmt.Println("Standard Fibonacci:")
	for _, f := range fibList {
		if f%2 == 0 {
			fmt.Println(f, "is even")
		} else {
			fmt.Println(f, "is odd")
		}
	}

	// calculate fibonacci sequence to specified length with recursion and
	// find nth value of fibonacci sequence
	n := 23
	_, fibListRecursive := fibSeqRecursive(n, nil)
	fmt.Println("Recursive Fibonacci at position", n, "is", fibListRecursive[n-1])
}
