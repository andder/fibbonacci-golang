package main

func fibSeq(fibLen int) []int {
	fibList := []int{0, 1}
	if fibLen > 2 {
		for i := 2; i <= fibLen; i++ {
			nextVal := fibList[i-2] + fibList[i-1]
			fibList = append(fibList, nextVal)
		}
	}
	return fibList
}

func fibSeqRecursive(fibLen int, sequence []int) (int, []int) {
	if sequence == nil {
		sequence = []int{0, 1}
	}
	nextVal := sequence[len(sequence)-2] + sequence[len(sequence)-1]
	sequence = append(sequence, nextVal)
	if len(sequence) < fibLen {
		return fibSeqRecursive(fibLen, sequence)
	}
	return fibLen, sequence
}
